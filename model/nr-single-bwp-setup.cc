/* Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; */
/*
 *   Copyright (c) 2019 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2 as
 *   published by the Free Software Foundation;
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "nr-single-bwp-setup.h"
#include <ns3/nr-module.h>
#include <ns3/object-map.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/nr-lbt-access-manager.h>
#include <ns3/nr-on-off-access-manager.h>

namespace ns3 {

static void
AttachToClosestEnb (Ptr<MmWaveHelper> helper, Ptr<NetDevice> ueDevice, NetDeviceContainer enbDevices)
{
  NS_ASSERT_MSG (enbDevices.GetN () > 0, "empty enb device container");
  Vector uepos = ueDevice->GetNode ()->GetObject<MobilityModel> ()->GetPosition ();
  double minDistance = std::numeric_limits<double>::infinity ();
  Ptr<NetDevice> closestEnbDevice;
  for (NetDeviceContainer::Iterator i = enbDevices.Begin (); i != enbDevices.End (); ++i)
    {
      Vector enbpos = (*i)->GetNode ()->GetObject<MobilityModel> ()->GetPosition ();
      double distance = CalculateDistance (uepos, enbpos);
      if (distance < minDistance)
        {
          minDistance = distance;
          closestEnbDevice = *i;
        }
    }
  NS_ASSERT (closestEnbDevice != nullptr);

  std::cout << "Attaching " << Names::FindName (ueDevice->GetNode()) << " to " <<
               Names::FindName (closestEnbDevice->GetNode()) << std::endl;

  helper->AttachToEnb (ueDevice, closestEnbDevice);
}

static void
AttachToClosestEnb (Ptr<MmWaveHelper> helper, NetDeviceContainer ueDevices, NetDeviceContainer enbDevices)
{
  for (NetDeviceContainer::Iterator i = ueDevices.Begin (); i != ueDevices.End (); i++)
    {
      AttachToClosestEnb (helper, *i, enbDevices);
    }
}

NrSingleBwpSetup::NrSingleBwpSetup (const NodeContainer &gnbNodes, const NodeContainer &ueNodes,
                                    const Ptr<SpectrumChannel> &channel,
                                    const Ptr<PropagationLossModel> &propagation,
                                    const Ptr<MmWave3gppChannel> &spectrumPropagation,
                                    double freq, double bw, uint32_t num, double gnbTxPower, double ueTxPower,
                                    const std::unordered_map<uint32_t, uint32_t> &ueGnbMap,
                                    const std::string &gnbCamType, const std::string &ueCamType,
                                    const std::string &scheduler)
  : L2Setup (gnbNodes, ueNodes), m_ueGnbMap (ueGnbMap)
{
  std::cout << "Configuring NR with " << gnbNodes.GetN ()
            << " gnbs, and " << ueNodes.GetN () << " UEs" << std::endl;

  for (auto it = gnbNodes.Begin(); it != gnbNodes.End(); ++it)
    {
      std::stringstream ss;
      ss << "GNB-NR-" << (*it)->GetId();
      Names::Add(ss.str (), *it);
    }

  for (auto it = ueNodes.Begin(); it != ueNodes.End(); ++it)
    {
      std::stringstream ss;
      ss << "UE-NR-" << (*it)->GetId();
      Names::Add(ss.str (), *it);
    }

  // setup the NR simulation
  m_helper = CreateObject<MmWaveHelper> ();
  m_helper->SetAttribute ("PathlossModel", StringValue ("ns3::MmWave3gppPropagationLossModel"));
  m_helper->SetAttribute ("ChannelModel", StringValue ("ns3::MmWave3gppChannel"));

  Ptr<MmWavePhyMacCommon> phyMacCommonBwp1 = CreateObject<MmWavePhyMacCommon>();
  phyMacCommonBwp1->SetCentreFrequency (freq);
  phyMacCommonBwp1->SetBandwidth (bw);
  phyMacCommonBwp1->SetNumerology (num);
  phyMacCommonBwp1->SetAttribute ("MacSchedulerType", TypeIdValue (TypeId::LookupByName(scheduler)));
  phyMacCommonBwp1->SetCcId (0);

  BandwidthPartRepresentation repr1 (0, phyMacCommonBwp1, channel, propagation, spectrumPropagation);
  repr1.m_gnbChannelAccessManagerType = gnbCamType;
  repr1.m_ueChannelAccessManagerType = ueCamType;
  m_helper->AddBandwidthPart (0, repr1);

  m_epcHelper = CreateObject<NrPointToPointEpcHelper> ();
  m_helper->SetEpcHelper (m_epcHelper);
  m_helper->Initialize ();

  // install mmWave net devices
  m_gnbDev = m_helper->InstallEnbDevice (GetGnbNodes ());
  m_ueDev = m_helper->InstallUeDevice (GetUeNodes ());

  double gnbX = pow (10, gnbTxPower / 10);
  double ueX = pow (10, ueTxPower / 10);

  double totalBandwidth = bw;

  for (uint32_t j = 0; j < m_gnbDev.GetN (); ++j)
    {
      ObjectMapValue objectMapValue;
      Ptr<MmWaveEnbNetDevice> netDevice = DynamicCast<MmWaveEnbNetDevice> (m_gnbDev.Get (j));
      netDevice->GetAttribute ("ComponentCarrierMap", objectMapValue);
      for (uint32_t i = 0; i < objectMapValue.GetN (); i++)
        {
          Ptr<ComponentCarrierGnb> bandwidthPart = DynamicCast<ComponentCarrierGnb> (objectMapValue.Get (i));
          if (i == 0)
            {
              bandwidthPart->GetPhy ()->SetTxPower (10 * log10 ((bw / totalBandwidth) * gnbX));
            }
          else
            {
              NS_FATAL_ERROR ("\n Please extend power assignment for additional bandwidth parts...");
            }
        }
      Ptr<MmWaveSpectrumPhy> gnbSpectrumPhy = netDevice->GetPhy (0)->GetSpectrumPhy ();
      std::stringstream nodeId;
      nodeId << netDevice->GetNode ()->GetId ();
      gnbSpectrumPhy->TraceConnect ("RxPacketTraceEnb", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::EnbReception, this));
      gnbSpectrumPhy->TraceConnect ("TxDataTrace", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::TxDataTrace, this));
      gnbSpectrumPhy->TraceConnect ("TxCtrlTrace", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::TxCtrlTrace, this));
    }

  m_ueNum = m_ueDev.GetN ();

  for (uint32_t i = 0; i < m_ueDev.GetN (); ++i)
    {
      ObjectMapValue objectMapValue;
      Ptr<MmWaveUeNetDevice> netDevice = DynamicCast<MmWaveUeNetDevice> (m_ueDev.Get (i));
      netDevice->GetAttribute ("ComponentCarrierMapUe", objectMapValue);

      for (uint32_t i = 0; i < objectMapValue.GetN (); i++)
        {
          Ptr<ComponentCarrierMmWaveUe> bandwidthPart = DynamicCast<ComponentCarrierMmWaveUe> (objectMapValue.Get (i));
          if (i == 0)
            {
              bandwidthPart->GetPhy ()->SetTxPower (10 * log10 ((bw / totalBandwidth) * ueX));
            }
          else
            {
              NS_FATAL_ERROR ("\n Please extend power assignment for additional bandwidth parts...");
            }
         }

      Ptr<MmWaveSpectrumPhy> ue1SpectrumPhy = DynamicCast<MmWaveUeNetDevice>
          (m_ueDev.Get (i))->GetPhy (0)->GetSpectrumPhy ();

      std::stringstream nodeId;
      nodeId << m_ueDev.Get (i)->GetNode ()->GetId ();

      ue1SpectrumPhy->TraceConnect ("RxPacketTraceUe", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::UeReception, this));
      ue1SpectrumPhy->TraceConnect ("TxDataTrace", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::TxDataTrace, this));
      ue1SpectrumPhy->TraceConnect ("TxCtrlTrace", nodeId.str (),
                                    MakeCallback (&NrSingleBwpSetup::TxCtrlTrace, this));
    }
}

NrSingleBwpSetup::~NrSingleBwpSetup ()
{

}

void
NrSingleBwpSetup::ConnectToRemotes (const NodeContainer &remoteHosts, const std::string &base)
{
  // create the internet and install the IP stack on the UEs
  // get SGW/PGW and create a single RemoteHost
  Ptr<Node> pgw = m_epcHelper->GetPgwNode ();
  // connect a remoteHost to pgw. Setup routing too
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (2500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.000)));

  std::cout << "Connecting remote hosts to EPC with a 100 Gb/s link, MTU 2500 B, delay 0 s" << std::endl;

  NetDeviceContainer internetDevices;
  for (auto it = remoteHosts.Begin (); it != remoteHosts.End (); ++it)
    {
      internetDevices.Add (p2ph.Install (pgw, *it));
    }

  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase (base.c_str (), "255.255.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
}

Ipv4InterfaceContainer
NrSingleBwpSetup::AssignIpv4ToUe (const std::unique_ptr<Ipv4AddressHelper> &address) const
{
  NS_UNUSED (address);
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  InternetStackHelper internet;
  internet.Install (GetUeNodes ());
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = m_epcHelper->AssignUeIpv4Address (GetUeDev ());

  // Set the default gateway for the UEs
  for (uint32_t j = 0; j < GetUeNodes ().GetN (); ++j)
    {
      auto ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (GetUeNodes ().Get (j)->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (m_epcHelper->GetUeDefaultGatewayAddress (), 1);
    }


  // if no map is provided, then attach UEs to the closest eNB
  if (m_ueGnbMap.empty ())
    {
      std::cout << "Strategy for UEs attachment: to the closest gNB. " << std::endl;
      AttachToClosestEnb (m_helper, GetUeDev (), GetGnbDev ());
    }
  else
    {
      std::cout << "Strategy for UEs attachment: manual mapping. " << std::endl;

      for (const auto & v : m_ueGnbMap)
        {
          if (v.first < GetUeDev ().GetN () && v.second < GetGnbDev ().GetN ())
            {
              std::cout << " attaching " << v.first << " to " << v.second << std::endl;
              m_helper->AttachToEnb (GetUeDev ().Get (v.first), GetGnbDev ().Get (v.second));
            }
        }
    }

  return ueIpIface;
}

Ipv4InterfaceContainer
NrSingleBwpSetup::AssignIpv4ToStations (const std::unique_ptr<Ipv4AddressHelper> &address) const
{
  NS_UNUSED (address);
  return Ipv4InterfaceContainer ();
}

void
NrSingleBwpSetup::TxDataTrace (std::string context, Time t)
{
  if (!m_channelOccupancyTimeCb.IsNull ())
    {
      m_channelOccupancyTimeCb (static_cast<uint32_t> (std::stoul (context)), t);
    }
}

void
NrSingleBwpSetup::TxCtrlTrace (std::string context, Time t)
{
  if (!m_channelOccupancyTimeCb.IsNull ())
    {
      m_channelOccupancyTimeCb (static_cast<uint32_t> (std::stoul (context)), t);
    }
}

void
NrSingleBwpSetup::EnbReception (std::string context, RxPacketTraceParams params)
{

  if (!m_sinrCb.IsNull ())
    {
      m_sinrCb (static_cast<uint32_t> (std::stoul (context)), params.m_sinr);
    }
  if (!m_macTxDataFailedCb.IsNull ())
    {
      if (params.m_corrupt)
        {
          m_macTxDataFailedCb (static_cast<uint32_t> (std::stoul (context)), params.m_tbSize);
        }
    }
}

void
NrSingleBwpSetup::UeReception (std::string context, RxPacketTraceParams params)
{
  if (!m_sinrCb.IsNull ())
    {
      m_sinrCb (static_cast<uint32_t> (std::stoul (context)), params.m_sinr);
    }

  if (!m_macTxDataFailedCb.IsNull ())
    {
      if (params.m_corrupt)
        {
          m_macTxDataFailedCb (static_cast<uint32_t> (std::stoul (context)), params.m_tbSize);
        }
    }
}

} // namespace ns3
